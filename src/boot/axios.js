import Vue from "vue";
import axios from "axios";

// let axiosInstance = axios.create({
//   baseURL: "https://athomedoc-cms.nbhood.com/api"
// });

// axiosInstance.interceptors.request.use(
//   function(config) {
//     console.log(config);
//     return config;
//   },
//   function(error) {
//     console.log(error);
//   }
// );

// // Add a response interceptor
// axiosInstance.interceptors.response.use(
//   function(response) {
//     console.log(response);
//   },
//   function(error) {
//     console.log(error);
//   }
// );

// export default axiosInstance;

// // axiosInstance.defaults.baseURL = "https://api.example.com";
// // axiosInstance.defaults.headers.common["Authorization"] = AUTH_TOKEN;
// // axiosInstance.defaults.headers.post["Content-Type"] =
// //   "application/x-www-form-urlencoded";
Vue.prototype.$axios = axios;
