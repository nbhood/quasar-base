import axios from "axios";

const state = () => ({
  homepage: "",
  doctors: "",
  illness: ""
});

const getters = {};

const mutations = {
  setHomepage(state, data) {
    state.homepage = data;
  },
  setIllness(state, data) {
    state.illness = data;
  },
  setDoctors(state, data) {
    state.doctors = data;
  }
};

const actions = {
  getHomepage({ commit }) {
    return axios
      .get(
        `${process.env.COUNTRY_CMS_LINK}/singletons/get/landing?token=1ed9556112a43cd1893dff05936ec2`
      )
      .then(data => {
        console.log(process.env.COUNTRY_CMS_LINK);
        commit("setHomepage", data.data);
      });
  },
  getDoctors({ commit }) {
    return axios
      .get(
        `${process.env.COUNTRY_CMS_LINK}/singletons/get/doctors?token=1ed9556112a43cd1893dff05936ec2`
      )
      .then(data => {
        console.log(process.env.COUNTRY_CMS_LINK);
        commit("setDoctors", data.data);
      });
  },
  getIllness({ commit }) {
    return axios
      .get(
        `${process.env.COUNTRY_CMS_LINK}/singletons/get/illness?token=1ed9556112a43cd1893dff05936ec2`
      )
      .then(data => {
        console.log(process.env.COUNTRY_CMS_LINK);
        commit("setIllness", data.data);
      });
  }
};

export default {
  state,
  mutations,
  getters,
  actions
};
