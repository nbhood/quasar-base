import axios from "axios";

const state = () => ({
  address: "",
  symptoms: [],
  location: "",
  multimedia: []
});

const getters = {};

const mutations = {
  addMultimedia(state, multimedia) {
    state.multimedia.push(multimedia);
  },
  addSymptoms(state, symptoms) {
    state.symptoms = symptoms;
  },
  addAddress(state, address) {
    state.address = address;
  },
  addLocation(state, location) {
    state.location = location;
  }
  // state.user.voiceNote = user.voiceNote;
  // state.user.image = user.image;
  // state.user.address = user.address;
  // state.user.symptoms = user.symptoms;
  // state.cart.push(item);
};

const actions = {
  submitRequest({ commit, state }, user) {
    let symptomsString = "";
    state.symptoms.map((symptom, index) => {
      symptomsString += `${symptom}${
        index === state.symptoms.length - 1 ? "." : ","
      }`;
    });
    let form = {
      ...user,
      address: state.address,
      symptoms: state.symptoms,
      location: state.location,
      symptoms: symptomsString
    };
    if (state.multimedia.length > 0) {
      state.multimedia.map(item => {
        if (item.Image) {
          form.image = item.Image;
        }
        if (item.Recording) {
          form.recording = item.Recording;
        }
      });
    }
    console.log(form);
    axios
      .post(
        `${process.env.COUNTRY_CMS_LINK}/forms/submit/requests?token=1ed9556112a43cd1893dff05936ec2`,
        {
          form
        }
      )
      .then(success => {
        console.log(success);
      });
  },
  addLocation({ commit }, location) {
    commit("addLocation", location);
  },
  addAddress({ commit }, address) {
    commit("addAddress", address);
  },
  addMultimedia({ commit }, files) {
    commit("addMultimedia", files);
  },

  addSymptoms({ commit }, symptoms) {
    commit("addSymptoms", symptoms);
  }
};

export default {
  state,
  mutations,
  getters,
  actions
};
