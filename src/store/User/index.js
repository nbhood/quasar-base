const state = () => ({
  isUser: false,
  token: "",
  name: "",
  age: "",
  gender: "",
  insurance: "",
  mobile: ""
});

const getters = {};

const mutations = {
  addUser(state, user) {
    state.isUser = true;
    state.name = user.name;
    state.age = user.age;
    state.gender = user.gender;
    state.mobile = user.mobile;
    state.insurance = user.insurance;
    // state.cart.push(item);
  },
  addMobile(state, number) {
    state.mobile = number;
  },
  addToken(state, token) {
    state.token = token;
  }
};

const actions = {
  addUser({ commit }, user) {
    commit("addUser", user);
  },
  addToken({ commit }, token) {
    commit("addToken", token);
  },
  addMobile({ commit }, number) {
    commit("addMobile", number);
  }
};

export default {
  state,
  mutations,
  getters,
  actions
};
