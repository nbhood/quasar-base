export default {
  failed: "Ar",
  success: "Action was successful",
  lang: "English",
  home: {
    book: "احجز زيارتك الآن",
    payment: {
      title: "خيارات الدفع",
      options: [
        {
          title: "التأمين الطبي"
        },
        {
          title: "ادفع نقداً"
        },
        {
          title: "ادفع بالكارت الائتماني"
        }
      ]
    },
    explore: "اكتشف",
    exploreSub: "التخصصات والأمراض الأكثر شيوعاً ",
    meet: "تعرف",
    meetSub: "على أطبائنا والفريق الطبي",
    discount: "خصومات خاصة للأعضاء",
    contact: "اتصل بنا",
    contactSub: "أحصل على الأفضل لتكون صحتك أفضل ",
    whatsapp: "واتساب",
    about: "من نحن"
  },
  app: {
    request: "طلب",
    other: "المزيد",
    requestHome: "طلب زيارة منزلية",
    confirm: "تأكيد",
    symptoms: {
      help: "من بحاجة إلى مساعدتنا؟",
      feel: "ما هي الأعراض التي تشعر بها؟",
      more: "يمكنك أن تخبرنا بالمزيد",
      optional: "اختياري",
      record: "سجل صوت",
      recordFeel: "أخبرنا المزيد واترك لنا ملاحظة صوتية",
      upload: "حمل صورة",
      uploadSub: "صورة الجُرح، عَرَض أو تقرير طبي"
    },
    timeline: {
      title: "المدة الزمنية",
      minutes: "دقيقة",
      recieved: " تم استلام الطلب",
      call: "اتصال الفريق الطبي",
      assign: "تم تكليف طبيب",
      arrived: "وصل الطبيب",
      cancel: "إلغاء الزيارة"
    },
    illnesses: {
      title: "الأمراض الأكثر شيوعاً التي يقوم أطبائنا بعلاجها",
      practice: "عياداتنا الطبية"
    },
    doctors: {
      title: "تعرف على أطبائنا"
    },
    checkoutMsg: "500 جنيه مصري نقداً أو بطاقة ائتمانية",
    location: "أدخل عنوانك",
    address: "الآن من فضلك أدخل عنوانك بالتفصيل",
    addressPlaceholder: "العنوان"
  },
  form: {
    name: "الاسم الكامل",
    birthday: "تاريخ الميلاد",
    mobile: "رقم الهاتف",
    gender: "الجنس",
    male: "ذكر",
    female: "أنثى",
    relation: "القرابة",
    signIn: "التسجيل",
    signUp: "انشاء حساب",
    insurance: "لدي بالفعل بطاقة تأمينية",
    continue: "للاستمرار برجاء ادخال رقم هاتفك"
  }
};
