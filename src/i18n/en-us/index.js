export default {
  failed: "Action failed",
  success: "Action was successful",
  lang: "عربي",
  home: {
    book: "Book Your Visit Now",
    payment: {
      title: "Payment Options",
      options: [
        {
          title: "Bill Your insurance",
          image: "/images/landing1.png"
        },
        {
          title: "Pay in Cash",
          image: "/images/landing2.svg"
        },
        {
          title: "Pay by Card",
          image: "/images/landing3.svg"
        }
      ]
    },
    explore: "Explore",
    exploreSub: "Explore Specialities and most common illnesses.",
    meet: "Meet",
    meetSub: "Our doctors and medical operation team",
    discount: "Special Discounts for Members",
    contact: "Contact Us",
    contactSub: "Get better, stay better",
    whatsapp: "whatsapp",
    about: "About Us"
  },
  app: {
    other: "Other",
    request: "Request",
    requestHome: "Request a home visit",
    confirm: "Confirm",
    symptoms: {
      help: "Who needs our help?",
      feel: "How do you feel?",
      more: "If you can tell us more",
      optional: "optional",
      record: "Record",
      recordFeel: "Express more and leave us a voice note",
      upload: "Upload",
      uploadSub: "Picture of injury, symptom or a report"
    },
    timeline: {
      title: "Timeline",
      minutes: "min",
      recieved: "Request Received",
      call: "Medical Team Call",
      assign: "Assigned Doctor",
      arrived: "Doctor Arrived",
      cancel: "Cancel Visit"
    },
    illnesses: {
      title: "The most common illnesses our doctors treat",
      practice: "Our Clinical Practice"
    },
    doctors: {
      title: "Meet our doctors"
    },
    location: "Enter a Location",
    checkoutMsg: "EGP 500 Cash or Credit Card",
    address: "Now, please fill in your address details",
    addressPlaceholder: "Address"
  },
  form: {
    name: "Full Name",
    birthday: "Birthday",
    mobile: "Mobile Number",
    gender: "Gender",
    male: "Male",
    female: "Female",
    relation: "Relation",
    signIn: "Sign in",
    signUp: "Sign up",
    insurance: "I do have insurance card",
    continue: "To continue your request please fill in your mobile number"
  }
};
