import store from "../store/index";

const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/index.vue")
      }
    ]
  }
  // {
  //   path: "/app",
  //   component: () => import("layouts/appLayout.vue"),
  //   children: [
  //     {
  //       path: "",
  //       redirect: "symptoms"
  //     }
  //   ]
  // }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
